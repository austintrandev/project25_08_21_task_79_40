package com.shoppizza365.repository;

import org.springframework.data.jpa.repository.*;

import com.shoppizza365.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long> {

}
