package com.shoppizza365.repository;

import org.springframework.data.jpa.repository.*;

import com.shoppizza365.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {

}
