package com.shoppizza365.repository;

import org.springframework.data.jpa.repository.*;

import com.shoppizza365.model.COffice;

public interface IOfficeRepository extends JpaRepository<COffice, Long> {

}
