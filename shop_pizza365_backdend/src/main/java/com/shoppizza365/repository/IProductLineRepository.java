package com.shoppizza365.repository;

import org.springframework.data.jpa.repository.*;

import com.shoppizza365.model.CProductLine;

public interface IProductLineRepository extends JpaRepository<CProductLine, Long> {

}
